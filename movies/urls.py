from django.http import request
from django.http.response import HttpResponse
from django.urls import path
from . import views

urlpatterns = [
    path('about/', views.about, name='about'),
    path('', views.index, name='index'),
    path('movies/<int:movie_id>/', views.detail, name='detail'),
    path('movies/', views.list, name='list')
]