# from django.http.response import HttpResponse
from django.http import HttpResponse
from django.shortcuts import render
from .temp_data import movie_data

# Create your views here.
def index(request):
    context = {}
    return render(request, 'movies/index.html', context)

def about(request):
    context = {}
    return render(request, 'movies/about.html', context)

def detail(request, movie_id):
    context = {'movie': movie_data[movie_id - 1]}
    return render(request, 'movies/detail.html', context)

def list(request):
    context = {}
    if request.GET.get('query', False):
        context = {
            "movie_list": [
                m for m in movie_data
                if request.GET['query'].lower() in m['name'].lower()
            ]
        }
    else:
        context = {'movie_list': movie_data}
    return render(request, 'movies/list.html', context)